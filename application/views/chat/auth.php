<?php 
    $this->load->view('includes/header'); 
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <form class="form-signin" method="post" id="loginform" action="<?=base_url('Chat/autenticar')?>">
                <div class="form-group">
                    <img class="mx-auto d-block" src="https://i.ya-webdesign.com/images/pow-png-zap-4.png" alt="" width="120" height="120">
                </div>

                <div class="form-group">
                    <h1 class="h3 d-flex justify-content-center font-weight-normal">Autenticação</h1>
                </div>

                <div class="form-group">
                    <label for="email" class="">E-mail:</label>
                    <input type="email" id="email" name="email" class="form-control" placeholder="email@email.com.br" required autofocus>
                </div>
                
                <div class="form-group">
                    <button style="background: #ED1B24; border: none" class="btn btn-primary btn-block" type="submit">Entrar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php $this->load->view('includes/footer');?>