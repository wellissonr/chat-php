<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends CI_Controller {

	public function __construct() {

		parent::__construct();
        $this->load->model('Chat_Model', 'chat');
        $this->load->model('Users_Model', 'userm');
		$this->load->library('Controle_Acesso', 'controle_acesso');
    }
    
	public function index() {
		$this->dados['titulo'] = 'ChatZap';
		$this->load->view('chat/index', $this->dados);
    }
    
    public function auth() {
        $this->dados['titulo'] = 'Autenticaçao ChatZap';
		$this->load->view('chat/auth', $this->dados);
    }

    public function autenticar() {
        $email = addslashes(strtoupper($this->input->post("email")));

        $this->dados['logado'] = strtolower($email);
        
        if($email) {
            $this->load->view('chat/index', $this->dados);
        }
    }
}