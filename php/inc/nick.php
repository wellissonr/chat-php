<?php
	if($_SERVER['SCRIPT_NAME'] == "/chat-php/vermensagens.php") {
		echo "<script>location.href='../index.php'</script>";
	}

	require_once 'header.php';
	require_once 'config.php';

    unset(
        $_SESSION['usuarioId'],         
        $_SESSION['usuarioNome'],       
        $_SESSION['usuarioNivelAcesso'], 
        $_SESSION['usuarioLogin'],      
        $_SESSION['usuarioSenha']
    );
?>
<div class="container">
	<div class="row">
		<div class="col-md-5 col-lg-4 col-lg-offset-4">
			<h1 align="center">Bate Papo</h1>
			<div class="login-container">
				<div class="login-panel panel panel-primary" style="border: solid 1px #fff">
					<div class="panel-heading" style="background-color: #34495E;">
						<p style="text-align: center">Autenticação</p>
					</div>
					<div class="panel-body">
						<form method="POST" action="inc/valida_login.php">
							<div class="form-group has-feedback">
                                <label class="control-label"></label>
								<input class="form-control" type="email" placeholder="Digite seu email.." name="email" id="email" required autofocus="">
							</div>
							<div class="form-group has-feedback">
                                <label class="control-label"></label>
								<input class="btn btn-success btn-block mb-10" type="submit" value="Entrar" style="background-color: #34495E; border: none;">
								<p class="text-danger text-center">
									<?php  
										if (isset($_SESSION['loginErro'])) {
											echo $_SESSION['loginErro'];
											unset($_SESSION['loginErro']);
										}
									?>
								</p>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php require_once 'footer.php'; ?>
