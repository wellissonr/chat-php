<?php 
	require_once("inc/config.php"); // Retorna o arquivo de configurações do site.
	if (isset($_SESSION['usuarioNome'])) {
		$query = "SELECT * FROM mensagens";
		$sql   = mysqli_query($con, $query);
        
        if (mysqli_num_rows($sql) > 0) {
			while($ln = mysqli_fetch_assoc($sql)){
				
				$nick     = strip_tags($ln['nick']);
				$mensagem = htmlspecialchars($ln['mensagem']);
				$dataHora = $ln['data_hora'];
                $newHora  = date('H:i', strtotime($dataHora));
                
                if($_SESSION['usuarioId'] == $ln['id']) {
                    echo "
                    <div class=\"row msg_container base_sent\">
                        <div class=\"col-md-8\">
                            <div class=\"messages msg_sent\">
                                <p style=\"color: #303030;\">$mensagem</p>
                                <br>
                                <time datetime=\"2009-11-13T20:00\" style=\"color: #303030\">$nick - $newHora</time>
                            </div>
                        </div>
                        <div class=\"col-md-2 col-xs-2 avatar\">
                            <!--<img src='img/profile.png' class=\"img-responsive\">-->
                        </div>
                    </div>";
                } else {
                    echo "
                    <div class=\"row msg_container base_receive\">
                        <div class=\"col-md-2 col-xs-2 avatar\">
                            <!--<img src='img/profile.png' class=\"img-responsive\">-->
                        </div>
                        <div class=\"col-md-8 col-md-offset-2\">
                            <div class=\"messages msg_receive\" style=\"background: #DCF8C6\">
                                <p style=\"color: #303030\">$mensagem</p>
                                <br>
                                <time datetime=\"2009-11-13T20:00\" style=\"color: #303030\">$nick - $newHora</time>
                            </div>
                        </div>
                    </div>";
                }
				
			}
		} else {
			echo "<p style=\"color: #303030\">Aguardando início da conversa...</p>";
		}
	} else {
		require_once("inc/nick.php"); // Retorna o arquivo para definir um nick
	}
?>