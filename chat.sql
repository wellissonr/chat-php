/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.6.41-log : Database - chat
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`chat` /*!40100 DEFAULT CHARACTER SET latin1 */;

/*Table structure for table `mensagens` */

DROP TABLE IF EXISTS `mensagens`;

CREATE TABLE `mensagens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_de` int(11) NOT NULL,
  `id_para` int(11) NOT NULL,
  `nick` varchar(100) DEFAULT '',
  `mensagem` varchar(255) DEFAULT '',
  `ip` varchar(50) DEFAULT '',
  `data_hora` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `mensagens` */

insert  into `mensagens`(`id`,`id_de`,`id_para`,`nick`,`mensagem`,`ip`,`data_hora`) values (2,1,1,'Wellisson Ribeiro','teste','::1','2019-07-25 11:02:10'),(3,2,2,'Hamon','fsdfdf','::1','2019-07-25 11:02:31'),(4,2,2,'Hamon','dfsdfsdf','::1','2019-07-25 11:49:09'),(5,2,2,'Hamon','dsfsdfsdf','::1','2019-07-25 11:50:20');

/*Table structure for table `usuario` */

DROP TABLE IF EXISTS `usuario`;

CREATE TABLE `usuario` (
  `cod_usuario` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `senha` longtext NOT NULL,
  `dt_ult_acesso` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `usuario` */

/*Table structure for table `usuarios` */

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nome` varchar(220) NOT NULL,
  `email` varchar(220) NOT NULL,
  `login` varchar(50) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `nivel_acesso_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `inicio` datetime NOT NULL,
  `limite` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `usuarios` */

insert  into `usuarios`(`id`,`nome`,`email`,`login`,`senha`,`nivel_acesso_id`,`created`,`modified`,`inicio`,`limite`) values (1,'Wellisson Ribeiro','welleh10@gmail.com','wellisson','e803adae1f5acc155699ad43e9b77629',1,'2017-01-16 10:09:24','2017-03-20 14:49:41','2019-07-25 10:36:28','2019-07-25 10:37:28'),(2,'Hamon','hamon@hamon.com.br','hamon','e8d95a51f3af4a3b134bf6bb680a213a',1,'2019-07-25 10:58:13','2019-07-25 10:58:16','2019-07-25 11:02:28','2019-07-25 11:03:28');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
